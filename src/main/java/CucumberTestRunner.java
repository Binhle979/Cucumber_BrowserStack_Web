import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        features = "src/main/resources/features/Magento.feature",
        plugin = { "pretty"}
)
public class CucumberTestRunner extends AbstractTestNGCucumberTests {
}
