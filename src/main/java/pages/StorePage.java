package pages;

import core.BasePage;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import java.util.List;

public class StorePage extends BasePage {


    @FindBy(id = "menu-magento-backend-stores")
    private WebElement storeLink;

    @FindBy(xpath = "//ul[@role='menu']/li[contains(@class,'item-catalog-attributes-attributes')]")
    private WebElement productLinkInStoreLink ;

    @FindBy(id = "add")
    private WebElement addNewAttributeButton;

    @FindBy(xpath = "//button[@title='Reset Filter']")
    private WebElement resetFilterLink;

    @FindBy(id = "attributeGrid_table")
    private WebElement productAttributeTable;

    @FindBy(xpath = "//div[@id='attributeGrid']//button[@title='Search']")
    private WebElement searchButton;

    @FindBy(id = "attributeGrid_page-limit")
    private WebElement numberRowOfTable;

    @FindBy(id = "attribute_label")
    private WebElement defaultLableText;

    @FindBy(id = "frontend_input")
    private WebElement catalogInputType;

    @FindBy(id = "is_required")
    private WebElement valueRequiredCombobox;

    @FindBy(xpath = "//span[text()='Advanced Attribute Properties']/parent::strong[@class='admin__collapsible-title']")
    private WebElement advancedAttributeButton;

    @FindBy(id = "attribute_code")
    private WebElement attributeCode;
    @FindBy(id = "is_global")
    private WebElement scope;
    @FindBy(id = "attribute_label")
    private WebElement defaultValue;

    @FindBy(id = "is_unique")
    private WebElement uniqueValue;
    @FindBy(id = "frontend_class")
    private WebElement validationForStoreOwner;

    @FindBy(id = "save")
    private WebElement saveButton;

    @FindBy(xpath = "//main[@id='anchor-content']//div[@class='messages']/div/div")
    private WebElement message;

    @FindBy(id="attributeGrid_filter_attribute_code")
    private  WebElement attributeCodeTable;

    @FindAll({@FindBy(how= How.XPATH,using="//table[@id='attributeGrid_table']//tbody/tr")} )
    private List<WebElement> numberOfRow;

    @FindBy(xpath = "//table[@id='attributeGrid_table']//tbody/tr")
    private WebElement row;

    @FindBy(id = "attribute_label")
    private WebElement getDefaultLableText;

    public StorePage(WebDriver driver) {
        super(driver);
    }

    public void clickToStoreLink(){
        getWebDriverWait().until(ExpectedConditions.visibilityOf(storeLink));
        storeLink.click();
    }

    public void clickToProducLinkInStoreLink(){
        getWebDriverWait().until(ExpectedConditions.visibilityOf(productLinkInStoreLink));
        productLinkInStoreLink.click();
    }

    public void verifyFiledInPageDisplayed() {
        getWebDriverWait().until(ExpectedConditions.visibilityOf(addNewAttributeButton));
        Assert.assertTrue(addNewAttributeButton.isDisplayed());
        Assert.assertTrue(resetFilterLink.isDisplayed());
        Assert.assertTrue(productAttributeTable.isDisplayed());
        Assert.assertTrue(searchButton.isDisplayed());
        Select select=new Select(numberRowOfTable);
        Assert.assertEquals(select.getFirstSelectedOption().getText(),"20");
    }

    public void clickAddNewAttributeButton() {
        getWebDriverWait().until(ExpectedConditions.visibilityOf(addNewAttributeButton));
        addNewAttributeButton.click();
    }

    public void verifySomeFiledInNewAttributePageDisplayed() {
        getWebDriverWait().until(ExpectedConditions.visibilityOf(defaultLableText));
        Assert.assertTrue(defaultLableText.isDisplayed());
        Assert.assertTrue(catalogInputType.isDisplayed());
        Assert.assertTrue(valueRequiredCombobox.isDisplayed());
    }

    public void clickToAdvancedAttribute() {
        getWebDriverWait().until(ExpectedConditions.visibilityOf(advancedAttributeButton));
        advancedAttributeButton.click();
    }

    public void verifyFiledInAdvancedAttributeDisplayed() {
        getWebDriverWait().until(ExpectedConditions.visibilityOf(attributeCode));
        Assert.assertTrue(attributeCode.isDisplayed());
        Assert.assertTrue(scope.isDisplayed());
        Assert.assertTrue(defaultValue.isDisplayed());
        Assert.assertTrue(uniqueValue.isDisplayed());
        Assert.assertTrue(validationForStoreOwner.isDisplayed());
    }

    public void senkeyToDefaulValue(String value) {
        getWebDriverWait().until(ExpectedConditions.visibilityOf(defaultValue));
        defaultValue.sendKeys(value);
    }

    public void selectCatalogtype(String value) {
        Select select=new Select(catalogInputType);
        select.selectByVisibleText(value);
    }

    public void selectValueRequired(String value) {
        Select select=new Select(valueRequiredCombobox);
        select.selectByVisibleText(value);
    }

    public void clickToSave() {
        getWebDriverWait().until(ExpectedConditions.visibilityOf(saveButton));
        saveButton.click();
    }

    public String getTextMessageSuccefully() {
        getWebDriverWait().until(ExpectedConditions.visibilityOf(message));
        return message.getText();
    }

    public void senKeyToAttributeCodeTable(String value) {
        getWebDriverWait().until(ExpectedConditions.visibilityOf(attributeCodeTable));
        attributeCodeTable.sendKeys(value);
        attributeCodeTable.sendKeys(Keys.ENTER);
    }

    public String verifyOneRowDisplayed() {
        return String.valueOf(numberOfRow.size());
    }

    public void clickRow() {
        getWebDriverWait().until(ExpectedConditions.visibilityOf(row));
        row.click();
    }

    public String getDefaultLabel() {
        getWebDriverWait().until(ExpectedConditions.visibilityOf(getDefaultLableText));
        return  getDefaultLableText.getAttribute("value");
    }
}
