package pages;

import core.BasePage;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

public class CatalogPage extends BasePage {

    @FindBy(id = "menu-magento-catalog-catalog")
    private WebElement catalogLink;

    @FindBy(xpath="//ul[@role='menu']/li[contains(@class,'item-catalog-products')]/a")
    private WebElement  productLink;

    @FindBy(id = "fulltext")
    private WebElement searchInput;

    @FindBy(xpath = "//a[text()='Edit']")
    private WebElement editBtn;

    @FindBy(xpath = "//input[@name='product[name]']")
    private WebElement productName;

    @FindBy(name ="product[price]")
    private WebElement productPrice;

    @FindBy(xpath = "//div[@class='spinner']")
    private WebElement spinner;

    public CatalogPage(WebDriver driver) {
        super(driver);
    }


    public void clickToCatalogLink() {
        getWebDriverWait().until(ExpectedConditions.visibilityOf(catalogLink));
        catalogLink.click();
    }

    public void clickToProductLink(){
        getWebDriverWait().until(ExpectedConditions.visibilityOf(productLink));
        productLink.click();
    }

    public void sendKeyToSearchTextBox(String keyword) {
        getWebDriverWait().until(ExpectedConditions.visibilityOf(searchInput));
        searchInput.clear();
        searchInput.sendKeys(keyword);
    }

    public void clickSearchButton() {
        searchInput.sendKeys(Keys.ENTER);
    }

    public void clickEditButton(){
        getWebDriverWait().until(ExpectedConditions.elementToBeClickable(editBtn));
        editBtn.click();
    }

    public void verifyProduct(String name, String price , String attributeSet){
        getWebDriverWait().until(ExpectedConditions.invisibilityOf(spinner));
        Assert.assertEquals(productName.getAttribute("value"),name);
        Assert.assertEquals(productPrice.getAttribute("value"),price);
    }
}
