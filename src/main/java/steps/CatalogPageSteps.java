package steps;

import core.BaseTests;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pages.CatalogPage;

public class CatalogPageSteps extends BaseTests {
    CatalogPage catalogPage = new CatalogPage(getDriver());

    @Given("User click to the Catalog on the left menu then click to the Products")
    public void userClickToTheCatalogOnTheLeftMenuThenClickToTheProducts() {
        catalogPage.clickToCatalogLink();
        catalogPage.clickToProductLink();
    }

    @When("User input search key {string} and click search")
    public void userInputSearchKeyProductAndClickSearch(String product) {
        catalogPage.sendKeyToSearchTextBox(product);
        catalogPage.clickSearchButton();
    }

    @And("User click edit product")
    public void userClickEditProduct() {
        catalogPage.clickEditButton();
    }

    @Then("User would see the edit product page displayed with product name:{string} ,price:{string} ,attribute set:{string}")
    public void verifyProductDetail(String name, String price, String attributeSet) {
        catalogPage.verifyProduct(name, price, attributeSet);
    }
}
