package steps;

import core.BaseTests;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import pages.HomePage;

import java.util.List;
import java.util.Map;


public class HomePageSteps extends BaseTests {
    HomePage homePage = new HomePage(getDriver());

    @Given("User open the browser for website url and login with username and password")
    public void userHasOpenedMagentoWebsiteAndLogin(DataTable table) {
        List<Map<String,String>> data=table.asMaps(String.class,String.class);
        homePage.openMagento(data.get(0).get("url"));
        homePage.login(data.get(0).get("username"),data.get(0).get("password"));
    }

}
