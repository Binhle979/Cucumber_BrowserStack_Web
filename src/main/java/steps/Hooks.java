package steps;

import core.BaseTests;
import io.cucumber.java.After;
import io.cucumber.java.Before;

import java.net.MalformedURLException;

public class Hooks {
   BaseTests base= new BaseTests();
    @Before
    public void beforeSuite() throws MalformedURLException {
      base.openBrowser();
    }

    @After
    public void afterSuite(){
        base.closeBrowser();
    }
}