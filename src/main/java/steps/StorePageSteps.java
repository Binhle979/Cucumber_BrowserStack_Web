package steps;

import core.BaseTests;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;
import pages.StorePage;

import java.util.List;
import java.util.Map;

public class StorePageSteps extends BaseTests {

    StorePage storePage = new StorePage(getDriver());

    @Given("User click to the Store on the left menu")
    public void userClickToTheStoreOnTheLeftMenu() {
        storePage.clickToStoreLink();
    }

    @When("User click to the Product")
    public void userClickToTheProduct() {
        storePage.clickToProducLinkInStoreLink();
    }

    @Then("User would see Product Attributes screen displayed with some element: Add New Attribute button,Search button,Reset Filter link")
    public void verifyProductAttributeScreen() {
        storePage.verifyFiledInPageDisplayed();
    }

    @And("User click to the Add New Attribute button and scroll down to Advanced Attribute Properties")
    public void userClickToTheAddNewAttributeButton() {
        storePage.clickAddNewAttributeButton();
    }

    @Then("User would see The Add New Attribute displayed with some field :Default Label text,Catalog Input Type for Store Owner combobox")
    public void verifyAddNewAttributeScreen() {
        storePage.verifySomeFiledInNewAttributePageDisplayed();
    }

    @And("Advanced Attribute Properties display with some field : Attribute Code,Scope,Default Value,Unique Value,Input Validation for Store Owner,...")
    public void verifyAdvancedAddNewAttributeScreen() {
        storePage.clickToAdvancedAttribute();
        storePage.verifyFiledInAdvancedAttributeDisplayed();
    }

    @And("User click to the Add New Attribute button and input Default Label,Catalog Input Type for Store Owner and Values Required")
    public void userClickToTheAddNewAttributeButtonAndInputDefault(DataTable dataTable) {
        List<Map<String,String>> data=dataTable.asMaps(String.class,String.class);
        storePage.clickAddNewAttributeButton();
        storePage.senkeyToDefaulValue(data.get(0).get("Default Label"));
        storePage.selectCatalogtype(data.get(0).get("Catalog Input Type for Store Owner"));
        storePage.selectValueRequired(data.get(0).get("Values Required"));
    }

    @And("Click to the Save")
    public void clickToTheSave() {
        storePage.clickToSave();
    }

    @Then("User would see the message")
    public void verifyMessageSuccessful(DataTable dataTable) {
        List<Map<String,String>> data=dataTable.asMaps(String.class,String.class);
        Assert.assertEquals(storePage.getTextMessageSuccefully(),data.get(0).get("message"));
    }

    @And("User input Attribute Code: {string} then click to the row")
    public void userInputAttributeCodeThenClickToTheRow(String label) {
        storePage.senKeyToAttributeCodeTable(label);
    }

    @Then("The only one row with Attribute Code display")
    public void theOnlyOneRowWithAttributeCodeDisplayWithDefaultLabelIs() {
        storePage.verifyOneRowDisplayed();
    }

    @And("default Label is : {string}")
    public void defaultLabelIs(String label) {
        storePage.clickRow();
        Assert.assertEquals(storePage.getDefaultLabel(),label);
    }
}
