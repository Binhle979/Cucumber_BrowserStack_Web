Feature: Magento

  Background:
    Given User open the browser for website url and login with username and password
      |url                                                                             |username|password    |
      |https://demoshops.splendid-internet.de/magento/demoshop-magento2-daily/demoadmin|demoshop|demoshop2022|

    #Test case 1
  Scenario Outline: Verify search  Product page
    Given User click to the Catalog on the left menu then click to the Products
    When User input search key "<Product>" and click search
    And User click edit product
    Then User would see the edit product page displayed with product name:"<Product>" ,price:"<Price>" ,attribute set:"<Attribute Set>"
    Examples:
      | Product          | Price | Attribute Set |
      | Joust Duffle Bag | 34.00 | Bag           |

     #Test case 2
  Scenario: Verify Product Attributes page
    Given User click to the Store on the left menu
    When User click to the Product
    Then User would see Product Attributes screen displayed with some element: Add New Attribute button,Search button,Reset Filter link

     #Test case 3
  Scenario: Verify Add New Product Attributes page
    Given User click to the Store on the left menu
    When User click to the Product
    And User click to the Add New Attribute button and scroll down to Advanced Attribute Properties
    Then User would see The Add New Attribute displayed with some field :Default Label text,Catalog Input Type for Store Owner combobox
    And Advanced Attribute Properties display with some field : Attribute Code,Scope,Default Value,Unique Value,Input Validation for Store Owner,...

     #Test case 4
  Scenario: Verify Create New Product Attributes successfully
    Given User click to the Store on the left menu
    When User click to the Product
    And User click to the Add New Attribute button and input Default Label,Catalog Input Type for Store Owner and Values Required
    |Default Label|Catalog Input Type for Store Owner|Values Required|
    |Test1        |     Text Editor                  |      No       |
    And Click to the Save
    Then User would see the message
    |message                         |
    |You saved the product attribute.|

     #Test case 5
  Scenario Outline: Verify Search Product Attributes
    Given User click to the Store on the left menu
    When User click to the Product
    And User input Attribute Code: "<Attribute Code>" then click to the row
    Then The only one row with Attribute Code display
    And default Label is : "<Attribute Code>"
    Examples:
    |Attribute Code|
    |   Color      |